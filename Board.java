// just stores the moves

public class Board {
	private char board [][] = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
	
	/* getSpace method- see if space is occupied. returns boolean
	 * that indicates if space is filled or not. Used in GameLogic
	 * to check for valid moves
	 */
	public char getSpace(int xCoord, int yCoord){
		return board[xCoord][yCoord];
	}
	
	public Board () {
		
	}

	public void makeMove(char c, int i, int j) {
		board[i][j] = c;
	}
}
