import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.ActionListener;
import java.awt.Font;

public class TicTacToeGui {

	private JFrame frame;
	private final JButton btnNewButton_1 = new JButton("");
	private final Action action = new SwingAction();
	private final Action action_1 = new SwingAction_1();
	private final Action action_2 = new SwingAction_2();
	private final Action action_3 = new SwingAction_3();
	private final Action action_4 = new SwingAction_4();
	private final Action action_5 = new SwingAction_5();
	private final Action action_6 = new SwingAction_6();
	private final Action action_7 = new SwingAction_7();
	private final Action action_8 = new SwingAction_8();
	private char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
	private int count = 0;
	

	public boolean isFull() {
		boolean isFull = true;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (board[i][j] == '-')
					isFull = false;
			}
		}

		return isFull;
	}

	public boolean checkForWin() {
		return (checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin());
	}

	private boolean checkRowCol(char c1, char c2, char c3) {
		return ((c1 != '-') && (c1 == c2) && (c2 == c3));
	}

	private boolean checkRowsForWin() {
		for (int i = 0; i < 3; i++) {
			if (checkRowCol(board[i][0], board[i][1], board[i][2]) == true) {
				return true;
			}
		}
		return false;
	}

	private boolean checkColumnsForWin() {
		for (int i = 0; i < 3; i++) {
			if (checkRowCol(board[0][i], board[1][i], board[2][i]) == true) {
				return true;
			}
		}
		return false;
	}

	private boolean checkDiagonalsForWin() {
		return ((checkRowCol(board[0][0], board[1][1], board[2][2]) == true)
				|| (checkRowCol(board[0][2], board[1][1], board[2][0]) == true));
	}


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TicTacToeGui window = new TicTacToeGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TicTacToeGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 569, 440);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 3, 0, 0));

		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setFont(new Font("Tahoma", Font.PLAIN, 89));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (count % 2 == 0) {
					btnNewButton_3.setText("X");
					board[0][0] = 'x';
				}else {
					btnNewButton_3.setText("O");
					board[0][0] = 'o';
				}
				count++;
				if (checkForWin()) {
					JOptionPane.showMessageDialog(null,"Win!");
				}
				else if (isFull()) {
					JOptionPane.showMessageDialog(null, "Tie!");
				}
			}
		});
		btnNewButton_3.setAction(action);
		panel.add(btnNewButton_3);

		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 89));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (count % 2 == 0) {
					btnNewButton_2.setText("X");
					board[0][1] = 'x';
				}else {
					btnNewButton_2.setText("O");
					board[0][1] = 'o';
				}
				count++;
				
				if (checkForWin()) {
					JOptionPane.showMessageDialog(null,"Win!");
				}
				else if (isFull()) {
					JOptionPane.showMessageDialog(null, "Tie!");
				}
			}
		});
		btnNewButton_2.setAction(action_1);
		panel.add(btnNewButton_2);

		JButton btnNewButton = new JButton("");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 89));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (count % 2 == 0) {
					btnNewButton.setText("X");
					board[0][2] = 'x';
				}else {
					btnNewButton.setText("O");
					board[0][2] = 'o';
				}
				count++;
				
				if (checkForWin()) {
					JOptionPane.showMessageDialog(null,"Win!");
				}
				else if (isFull()) {
					JOptionPane.showMessageDialog(null, "Tie!");
				}
			}
		});
		btnNewButton.setAction(action_2);
		panel.add(btnNewButton);

		JButton btnNewButton_6 = new JButton("");
		btnNewButton_6.setFont(new Font("Tahoma", Font.PLAIN, 89));
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (count % 2 == 0) {
					btnNewButton_6.setText("X");
					board[1][0] = 'x';
				}else {
					btnNewButton_6.setText("O");
					board[1][0] = 'o';
				}
				count++;
				
				if (checkForWin()) {
					JOptionPane.showMessageDialog(null,"Win!");
				}
				else if (isFull()) {
					JOptionPane.showMessageDialog(null, "Tie!");
				}
			}
		});
		btnNewButton_6.setAction(action_3);
		panel.add(btnNewButton_6);

		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setFont(new Font("Tahoma", Font.PLAIN, 89));
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (count % 2 == 0) {
					btnNewButton_5.setText("X");
					board[1][1] = 'x';
				}else {
					btnNewButton_5.setText("O");
					board[1][1] = 'o';
				}
				count++;
				
				if (checkForWin()) {
					JOptionPane.showMessageDialog(null,"Win!");
				}
				else if (isFull()) {
					JOptionPane.showMessageDialog(null, "Tie!");
				}
			}
		});
		btnNewButton_5.setAction(action_4);
		panel.add(btnNewButton_5);

		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setFont(new Font("Tahoma", Font.PLAIN, 89));
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (count % 2 == 0) {
					btnNewButton_4.setText("X");
					board[1][2] = 'x';
				}else {
					btnNewButton_4.setText("O");
					board[1][2] = 'o';
				}
				count++;
				
				if (checkForWin()) {
					JOptionPane.showMessageDialog(null,"Win!");
				}
				else if (isFull()) {
					JOptionPane.showMessageDialog(null, "Tie!");
				}
			}
		});
		btnNewButton_4.setAction(action_5);
		panel.add(btnNewButton_4);

		JButton btnNewButton_7 = new JButton("");
		btnNewButton_7.setFont(new Font("Tahoma", Font.PLAIN, 89));
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (count % 2 == 0) {
					btnNewButton_7.setText("X");
					board[2][0] = 'x';
				}else {
					btnNewButton_7.setText("O");
					board[2][0] = 'o';
				}
				count++;
				
				if (checkForWin()) {
					JOptionPane.showMessageDialog(null,"Win!");
				}
				else if (isFull()) {
					JOptionPane.showMessageDialog(null, "Tie!");
				}
			}
		});
		btnNewButton_7.setAction(action_6);
		panel.add(btnNewButton_7);
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 89));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (count % 2 == 0) {
					btnNewButton_1.setText("X");
					board[2][1] = 'x';
				}else {
					btnNewButton_1.setText("O");
					board[2][1] = 'o';
				}
				count++;
				
				if (checkForWin()) {
					JOptionPane.showMessageDialog(null,"Win!");
				}
				else if (isFull()) {
					JOptionPane.showMessageDialog(null, "Tie!");
				}
			}
		});
		btnNewButton_1.setAction(action_7);
		panel.add(btnNewButton_1);

		JButton btnNewButton_8 = new JButton("");
		btnNewButton_8.setFont(new Font("Tahoma", Font.PLAIN, 89));
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (count % 2 == 0) {
					btnNewButton_8.setText("X");
					board[2][2] = 'x';
				}else {
					btnNewButton_8.setText("O");
					board[2][2] = 'o';
				}
				count++;			
				if (checkForWin()) {
					JOptionPane.showMessageDialog(null,"Win!");
				}
				else if (isFull()) {
					JOptionPane.showMessageDialog(null, "Tie!");
				}
			}
		});
		btnNewButton_8.setAction(action_8);
		panel.add(btnNewButton_8);

		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.NORTH);

		JLabel lblNewLabel = new JLabel("TicTacToe");
		panel_1.add(lblNewLabel);


	}

	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	private class SwingAction_1 extends AbstractAction {
		public SwingAction_1() {
			putValue(NAME, "");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	private class SwingAction_2 extends AbstractAction {
		public SwingAction_2() {
			putValue(NAME, "");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	private class SwingAction_3 extends AbstractAction {
		public SwingAction_3() {
			putValue(NAME, "");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	private class SwingAction_4 extends AbstractAction {
		public SwingAction_4() {
			putValue(NAME, "");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	private class SwingAction_5 extends AbstractAction {
		public SwingAction_5() {
			putValue(NAME, "");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	private class SwingAction_6 extends AbstractAction {
		public SwingAction_6() {
			putValue(NAME, "");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	private class SwingAction_7 extends AbstractAction {
		public SwingAction_7() {
			putValue(NAME, "");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	private class SwingAction_8 extends AbstractAction {
		public SwingAction_8() {
			putValue(NAME, "");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
