import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

class TicTacToeTest {
	
	private TicTacToe board;
	
	@Before
	public void setUp() {
		board = new TicTacToe();
	}
	
	@After
	public void down() {
		board = null;
	}
	
	@Test
	void testMark() {
		// position was available, set X
		board.setMark("X",0,0);
		assertEquals("X",board.getMark(0,0));
		// position was unavailable
		assertTrue(board.getMark(0,0).equals("X"));
		// position was available, set O
		board.setMark("O",1,1);
		assertEquals("O",board.getMark(1,1));
	}
	
	@Test
	void testWin() {
		String[][] w = new String[][] {{"X","X","X"},{"O","O","-"},{"-","-","-"}};
		String[][] w1 = new String[][] {{"O","X","X"},{"O","O","X"},{"X","X","O"}};
		String[][] fw = new String[][] {{"X","-","X"},{"O","O","-"},{"-","-","-"}};
		String[][] fw1 = new String[][] {{"O","X","O"},{"O","X","O"},{"X","O","O"}};
		
		TicTacToe b = new TicTacToe(w);
		TicTacToe b1 = new TicTacToe(w);
		TicTacToe b2 = new TicTacToe(w);
		TicTacToe b3 = new TicTacToe(w);
		
		//board not full , has winner
		assertTrue(b.isWin());
		//board full , has winner
		assertTrue(b1.isWin());
		//board not full , no winner
		assertFalse(b2.isWin());
		//board full , no winner
		assertFalse(b2.isWin());		
	}

}
