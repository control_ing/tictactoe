
public class CheckForWin2 {
	
	private char[][] board;
	
	public boolean isFull() {
		boolean isFull = true;

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (board[i][j] == '-')
					isFull = false;
			}
		}

		return isFull;
	}

	public boolean checkForWin() {
		return (checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin());
	}

	private boolean checkRowCol(char c1, char c2, char c3) {
		return ((c1 != '-') && (c1 == c2) && (c2 == c3));
	}

	private boolean checkRowsForWin() {
		for (int i = 0; i < 3; i++) {
			if (checkRowCol(board.getSpace(i,0), board.getSpace(i,1), board.getSPace(i,2)) == true) {
				return true;
			}
		}
		return false;
	}

	private boolean checkColumnsForWin() {
		for (int i = 0; i < 3; i++) {
			if (checkRowCol(board.getSpace(0,i), board.getSpace(1,i), board.getSPace(2,i)) == true) {
				return true;
			}
		}
		return false;
	}

	private boolean checkDiagonalsForWin() {
		return ((checkRowCol(board.getSpace(0,0), board.getSpace(1,1), board.getSpace(2,2)) == true)
				|| (checkRowCol(board.getSpace(0,2), board.getSpace(1,1), board.getSpace(2,0)) == true));
	}

}
